EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector-ML:RPi_GPIO J2
U 1 1 5516AE26
P 7500 2700
F 0 "J2" H 8250 2950 60  0000 C CNN
F 1 "RPi_GPIO" H 8250 2850 60  0000 C CNN
F 2 "RPi_Hat:Pin_Header_Straight_2x20" H 7500 2700 60  0001 C CNN
F 3 "" H 7500 2700 60  0000 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3100 7200 3100
Wire Wire Line
	7200 3100 7200 3900
Wire Wire Line
	7200 3900 7300 3900
Wire Wire Line
	7200 3900 7200 4600
Wire Wire Line
	7200 4600 7300 4600
Connection ~ 7200 3900
Wire Wire Line
	7200 4600 7200 4850
Wire Wire Line
	7200 4850 9300 4850
Wire Wire Line
	9300 4850 9300 4300
Wire Wire Line
	9300 4300 9200 4300
Connection ~ 7200 4600
Wire Wire Line
	9300 4300 9300 4100
Wire Wire Line
	9300 4100 9200 4100
Connection ~ 9300 4300
Wire Wire Line
	9300 4100 9300 3600
Wire Wire Line
	9300 3600 9200 3600
Connection ~ 9300 4100
Wire Wire Line
	9300 3600 9300 3300
Wire Wire Line
	9300 3300 9200 3300
Connection ~ 9300 3600
Wire Wire Line
	9300 3300 9300 2900
Wire Wire Line
	9300 2900 9200 2900
Connection ~ 9300 3300
$Sheet
S 1850 1700 2250 600 
U 60AFFDCF
F0 "Power Circuitry" 50
F1 "Power Circuitry.sch" 50
$EndSheet
$Sheet
S 1850 2600 2250 600 
U 60AFFE83
F0 "Amplifier Circuitry" 50
F1 "Amplifier Circuitry.sch" 50
$EndSheet
$Sheet
S 1850 3450 2250 650 
U 60AFFF62
F0 "LED Circuitry" 50
F1 "LED Circuitry.sch" 50
$EndSheet
Connection ~ 7200 4850
$Comp
L power:GND #PWR?
U 1 1 60BB70E8
P 7200 5300
AR Path="/60AFFDCF/60BB70E8" Ref="#PWR?"  Part="1" 
AR Path="/60BB70E8" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 7200 5050 50  0001 C CNN
F 1 "GND" H 7205 5127 50  0000 C CNN
F 2 "" H 7200 5300 50  0001 C CNN
F 3 "" H 7200 5300 50  0001 C CNN
	1    7200 5300
	1    0    0    -1  
$EndComp
Text GLabel 6450 5050 2    50   Output ~ 0
Vin
$Comp
L Connector:Jack-DC J3
U 1 1 60BB995B
P 6000 5150
F 0 "J3" H 6057 5475 50  0000 C CNN
F 1 "Jack-DC" H 6057 5384 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-063AH_Horizontal" H 6050 5110 50  0001 C CNN
F 3 "~" H 6050 5110 50  0001 C CNN
	1    6000 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4850 7200 5250
Wire Wire Line
	6300 5050 6450 5050
Wire Wire Line
	6300 5250 7200 5250
Connection ~ 7200 5250
Wire Wire Line
	7200 5250 7200 5300
Text GLabel 9500 2700 2    50   Input ~ 0
5V
Wire Wire Line
	9500 2700 9300 2700
Wire Wire Line
	9200 2800 9300 2800
Wire Wire Line
	9300 2800 9300 2700
Connection ~ 9300 2700
Wire Wire Line
	9300 2700 9200 2700
Text GLabel 6900 4100 0    50   Input ~ 0
Vadc
Wire Wire Line
	6900 4100 7300 4100
$EndSCHEMATC
