#=========================================================================================================
# How to use the micro-Hat
#=========================================================================================================

The UPS micro-Hat provides back-up power for the Pi-Zero. Below are the instructions for the devices use.

#=========================================================================================================
# Charging
#=========================================================================================================

    1. The micro-Hat battery pack must first be connected to the battery connectors.
    2. The micro-Hat can either be connected to the Pi-Zero or detached for seperate charging. Either way is 
       permissable.
    3. Ideally, the micro-Hat should not be loaded to maximum rating when charging but is capable of being 
       operated in this manner.
#=========================================================================================================
# Operations
#=========================================================================================================

    1. The micro-Hat must be connected to the Pi-Zero. 
    2. The batteries of the micro-Hat must be charged.
    3. The micro-Hat will provide back-up power when connected to the Pi-Zero so long as the batteries still 
       have energy stored.
    4. The micro-Hat should not be exposed to water.
    5. The micro-Hat, along with the Pi-Zero, should be placed in a area where they are not at risk of falling.
    6. The micro-Hat should not be operated at temepratures above 45 degrees Celsius as this will damage some of
       the components.
    7. The batteries should not be disposed of using fire as this poses an explosion risk.
    8. The batteries should be disposed of in line with safe disposal protocols.
    9. The batteries should not be short-circuited.
    10. Battery polarity should be observed before connecting.

#=========================================================================================================
